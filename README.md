**Teste Técnico Desenvolvedor .NET Fullstack Sênior**

**Recursos utilizados:**

- Patterns: DDD, TDD e Repository
- AutoMapper/DTO
- Documentação completa da API via Swagger
- ORM: EF Core
- BD: SQL Server

**Considerações:**

- Antes de executar a API, mudar a string de conexão no arquivo appsettings.json para o server que desejar
- Antes de executar os testes unitários, mudar a string de conexão no arquivo BaseTest para o server que desejar


