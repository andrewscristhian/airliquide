﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AirLiquide.Domain.Entities
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
