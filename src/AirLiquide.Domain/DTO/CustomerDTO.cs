﻿using AirLiquide.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace AirLiquide.Domain.DTO
{
    public class CustomerDTO : BaseEntity
    {
        [Required(ErrorMessage = "Informe o nome")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Informe a idade")]
        public int Age { get; set; }
    }
}
