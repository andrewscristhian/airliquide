﻿using AirLiquide.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace AirLiquide.Domain.Models
{
    public class Customer : BaseEntity
    {
        [Required(ErrorMessage = "Informe o nome")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Informe a idade")]
        public int Age { get; set; }
    }
}
