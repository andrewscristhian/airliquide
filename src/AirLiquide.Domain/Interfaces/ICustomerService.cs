﻿using AirLiquide.Domain.Models;
using System;
using System.Threading.Tasks;

namespace AirLiquide.Domain.Interfaces
{
    public interface ICustomerService : IRepository<Customer>
    {
        Task<bool> ExistAsync(Guid id);
    }
}
