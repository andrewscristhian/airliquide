﻿using AirLiquide.Domain.Models;
using AirLiquide.Test.IntegrationTest.Integration.Base;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AirLiquide.Test.IntegrationTest.Integration.CustomerIntegration
{
    public class CustomerRequests : BaseIntegration
    {
        private string _name { get; set; }
        private int _age { get; set; }

        [Fact]
        public async Task Execute_Customer()
        {
            _name = Faker.Name.First();
            _age = Faker.RandomNumber.Next();

            var customerData = new Customer()
            {
                Name = _name,
                Age = _age
            };

            //POST
            var response = await PostJsonAsync(customerData, $"{hostApi}v1/customer", client);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var postResult = await response.Content.ReadAsStringAsync();
            var recordCreated = JsonConvert.DeserializeObject<Customer>(postResult);

            //GET ALL
            response = await client.GetAsync($"{hostApi}v1/customer");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var jsonResult = await response.Content.ReadAsStringAsync();

            var customerUpdated = new Customer()
            {
                Id = recordCreated.Id,
                Name = Faker.Name.First(),
                Age = Faker.RandomNumber.Next()
            };

            //PUT
            var stringContent = new StringContent(JsonConvert.SerializeObject(customerUpdated),
                                    Encoding.UTF8, "application/json");
            response = await client.PutAsync($"{hostApi}v1/customer", stringContent);
            jsonResult = await response.Content.ReadAsStringAsync();
            var recordUpdated = JsonConvert.DeserializeObject<Customer>(jsonResult);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            //GET ID
            response = await client.GetAsync($"{hostApi}v1/customer/{recordUpdated.Id}");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            jsonResult = await response.Content.ReadAsStringAsync();
            var recordSelected = JsonConvert.DeserializeObject<Customer>(jsonResult);

            //DELETE
            response = await client.DeleteAsync($"{hostApi}v1/customer/{recordSelected.Id}");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

    }
}