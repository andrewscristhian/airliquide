﻿using AirLiquide.Application;
using AirLiquide.Infra.Data.Context;
using AirLiquide.Infra.Data.Mappings;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AirLiquide.Test.IntegrationTest.Integration.Base
{
    public class BaseIntegration : IDisposable
    {
        public AirLiquideContext context { get; private set; }
        public HttpClient client { get; private set; }
        public IMapper mapper { get; set; }
        public string hostApi { get; set; }
        public HttpResponseMessage response { get; set; }

        public BaseIntegration()
        {
            hostApi = "http://localhost:44389/api/v1";
            var builder = new WebHostBuilder()
               .UseEnvironment("Development")
               .UseStartup<Startup>();
            var server = new TestServer(builder);

            context = server.Host.Services.GetService(typeof(AirLiquideContext)) as AirLiquideContext;
            context.Database.Migrate();

            mapper = new AutoMapperFixture().GetMapper();

            client = server.CreateClient();
        }

        public static async Task<HttpResponseMessage> PostJsonAsync(object dataclass, string url, HttpClient client)
        {
            return await client.PostAsync(url,
                new StringContent(JsonConvert.SerializeObject(dataclass), System.Text.Encoding.UTF8, "application/json"));
        }

        public void Dispose()
        {
            context.Dispose();
            client.Dispose();
        }
    }

    public class AutoMapperFixture : IDisposable
    {
        public IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfiles>();
            });
            return config.CreateMapper();
        }
        public void Dispose() { }
    }
}

