﻿using Microsoft.Extensions.DependencyInjection;

namespace AirLiquide.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureCors
    {
        public static void AddCorsConfig(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyHeader()
                           .AllowAnyMethod();
                });
            });
        }
    }
}
