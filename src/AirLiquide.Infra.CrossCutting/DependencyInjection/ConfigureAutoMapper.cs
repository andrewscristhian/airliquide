﻿using AirLiquide.Infra.Data.Mappings;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace AirLiquide.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureAutoMapper<T> where T : class
    {
        public static void AddAutoMapperConfig(IServiceCollection services)
        {
            services.AddAutoMapper(c => c.AddProfile<AutoMapperProfiles>(), typeof(T));
        }
    }
}
