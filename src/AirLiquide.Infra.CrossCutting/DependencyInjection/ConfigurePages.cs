﻿using Microsoft.Extensions.DependencyInjection;

namespace AirLiquide.Infra.CrossCutting.DependencyInjection
{
    public class ConfigurePages
    {
        public static void AddPagesConfig(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddApiVersioning();
        }
    }
}
