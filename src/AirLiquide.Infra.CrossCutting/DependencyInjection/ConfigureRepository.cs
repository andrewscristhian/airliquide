﻿using AirLiquide.Domain.Interfaces;
using AirLiquide.Infra.Data.Context;
using AirLiquide.Infra.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AirLiquide.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureRepository
    {
        public static void AddDependenciesRepository(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddDbContext<AirLiquideContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("AirLiquideConnection")));
        }
    }
}
