﻿using AirLiquide.Infra.CrossCutting.Helpers.Swagger;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace AirLiquide.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureSwagger
    {
        public static void AddSwaggerConfig(IServiceCollection services)
        {
            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "AirLiquide-API",
                    Version = "v1",
                    Description = "Essas são as APIs para o teste AirLiquide",
                    Contact = new OpenApiContact()
                    {
                        Name = "Andrews Cristhian",
                        Email = "andrewsfogo@gmail.com",
                    }
                });

                config.OperationFilter<RemoveVersionParameterFilter>();
                config.DocumentFilter<ReplaceVersionWithExactValueInPathFilter>();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                config.IncludeXmlComments(xmlPath);
            });
        }
    }
}
