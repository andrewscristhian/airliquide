﻿using AirLiquide.Domain.Interfaces;
using AirLiquide.Service.Services;
using Microsoft.Extensions.DependencyInjection;

namespace AirLiquide.Infra.CrossCutting.DependencyInjection
{
    public class ConfigureService
    {
        public static void AddDependenciesService(IServiceCollection services)
        {
            services.AddTransient<ICustomerService, CustomerService>();
        }
    }
}
