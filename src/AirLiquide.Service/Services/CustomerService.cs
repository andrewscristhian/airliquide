﻿using AirLiquide.Domain.Interfaces;
using AirLiquide.Domain.Models;
using AirLiquide.Infra.Data.Context;
using AirLiquide.Infra.Data.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace AirLiquide.Service.Services
{
    public class CustomerService : Repository<Customer>, ICustomerService
    {
        public CustomerService(AirLiquideContext context) : base(context)
        { }

        public async Task<bool> ExistAsync(Guid id)
        {
            return await _context.Customers.AnyAsync(a => a.Id == id);
        }
    }
}
