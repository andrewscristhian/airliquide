﻿using AirLiquide.Domain.Interfaces;
using AirLiquide.Domain.Models;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace AirLiquide.Test.UnitTest.Service.CustomerTests
{
    public class GetCustomer : CustomerDataTests
    {
        private ICustomerService _service;
        private Mock<ICustomerService> _serviceMock;

        [Fact(DisplayName = "[Mock] Test GetId")]
        public async Task Execute_Get()
        {
            // Teste retorno Id existente
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.GetAsync(IdCustomer)).ReturnsAsync(customerData);
            _service = _serviceMock.Object;

            var result = await _service.GetAsync(IdCustomer);
            Assert.NotNull(result);
            Assert.True(result.Id == IdCustomer);
            Assert.Equal(NameCustomer, result.Name);

            // Teste retorno Id nulo
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.GetAsync(It.IsAny<Guid>())).Returns(Task.FromResult((Customer)null));
            _service = _serviceMock.Object;

            var _record = await _service.GetAsync(IdCustomer);
            Assert.Null(_record);
        }
    }
}
