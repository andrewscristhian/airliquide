﻿using AirLiquide.Domain.Interfaces;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace AirLiquide.Test.UnitTest.Service.CustomerTests
{
    public class UpdateCustomer : CustomerDataTests
    {
        private ICustomerService _service;
        private Mock<ICustomerService> _serviceMock;

        [Fact(DisplayName = "[Mock] Test Update")]
        public async Task Execute_Update()
        {
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.UpdateAsync(customerData)).ReturnsAsync(customerData);
            _service = _serviceMock.Object;

            var result = await _service.UpdateAsync(customerData);
            Assert.NotNull(result);
            Assert.Equal(NameCustomer, result.Name);
            Assert.Equal(AgeCustomer, result.Age);
        }
    }
}
