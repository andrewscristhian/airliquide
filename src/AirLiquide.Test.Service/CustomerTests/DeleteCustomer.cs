﻿using AirLiquide.Domain.Interfaces;
using AirLiquide.Domain.Models;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace AirLiquide.Test.UnitTest.Service.CustomerTests
{
    public class DeleteCustomer : CustomerDataTests
    {
        private ICustomerService _service;
        private Mock<ICustomerService> _serviceMock;

        [Fact(DisplayName = "[Mock] Test Delete")]
        public async Task Execute_Delete()
        {
            // Teste deleção id válido
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.DeleteAsync(customerData)).ReturnsAsync(true);
            _service = _serviceMock.Object;

            var result = await _service.DeleteAsync(customerData);
            Assert.True(result);

            // Teste deleção Id inválido
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.DeleteAsync(It.IsAny<Customer>())).ReturnsAsync(false);
            _service = _serviceMock.Object;

            result = await _service.DeleteAsync(new Customer());
            Assert.False(result);
        }
    }
}
