﻿using AirLiquide.Domain.Interfaces;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace AirLiquide.Test.UnitTest.Service.CustomerTests
{
    public class CreateCustomer : CustomerDataTests
    {
        private ICustomerService _service;
        private Mock<ICustomerService> _serviceMock;

        [Fact(DisplayName = "[Mock] Test Create")]
        public async Task Execute_Create()
        {
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.InsertAsync(customerData)).ReturnsAsync(customerData);
            _service = _serviceMock.Object;

            var result = await _service.InsertAsync(customerData);
            Assert.NotNull(result);
            Assert.Equal(NameCustomer, result.Name);
            Assert.Equal(AgeCustomer, result.Age);
        }
    }
}
