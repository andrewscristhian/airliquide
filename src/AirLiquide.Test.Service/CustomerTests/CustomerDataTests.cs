﻿using AirLiquide.Domain.Models;
using System;
using System.Collections.Generic;

namespace AirLiquide.Test.UnitTest.Service.CustomerTests
{
    public class CustomerDataTests
    {
        public static Guid IdCustomer { get; set; }
        public static string NameCustomer { get; set; }
        public static int AgeCustomer { get; set; }
        public List<Customer> listCustomer = new List<Customer>();
        public Customer customerData;

        public CustomerDataTests()
        {
            IdCustomer = Guid.NewGuid();
            NameCustomer = Faker.Name.First();
            AgeCustomer = Faker.RandomNumber.Next();


            for (int i = 0; i < 10; i++)
            {
                var customer = new Customer()
                {
                    Id = Guid.NewGuid(),
                    Name = Faker.Name.First(),
                    Age = Faker.RandomNumber.Next()
                };
                listCustomer.Add(customer);
            }

            customerData = new Customer
            {
                Id = IdCustomer,
                Name = NameCustomer,
                Age = AgeCustomer
            };
        }
    }
}
