﻿using AirLiquide.Domain.Interfaces;
using AirLiquide.Domain.Models;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirLiquide.Test.UnitTest.Service.CustomerTests
{
    public class GetAllCustomers : CustomerDataTests
    {
        private ICustomerService _service;
        private Mock<ICustomerService> _serviceMock;

        [Fact(DisplayName = "[Mock] Test GetAll")]
        public async Task Execute_GetAll()
        {
            // Teste retorno todos os dados
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.GetAllAsync(null, null, null)).ReturnsAsync(listCustomer);
            _service = _serviceMock.Object;

            var result = await _service.GetAllAsync();
            Assert.NotNull(result);
            Assert.True(result.Count() == 10);

            // Teste retorno lista nula
            var _listResult = new List<Customer>();
            _serviceMock = new Mock<ICustomerService>();
            _serviceMock.Setup(m => m.GetAllAsync(null, null, null)).ReturnsAsync(_listResult);
            _service = _serviceMock.Object;

            var _resultEmpty = await _service.GetAllAsync();
            Assert.Empty(_resultEmpty);
            Assert.True(_resultEmpty.Count() == 0);
        }
    }
}
