﻿using AirLiquide.Domain.DTO;
using AirLiquide.Domain.Models;
using AutoMapper;

namespace AirLiquide.Infra.Data.Mappings
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Customer, CustomerDTO>().ReverseMap();
        }
    }
}
