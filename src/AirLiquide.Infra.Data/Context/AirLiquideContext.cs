﻿using AirLiquide.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace AirLiquide.Infra.Data.Context
{
    public class AirLiquideContext : DbContext
    {
        public AirLiquideContext(DbContextOptions<AirLiquideContext> options) : base(options)
        { }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        { }
    }
}
