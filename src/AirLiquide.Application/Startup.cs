using AirLiquide.Infra.CrossCutting.DependencyInjection;
using AirLiquide.Infra.CrossCutting.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AirLiquide.Application
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigurePages.AddPagesConfig(services);
            ConfigureCors.AddCorsConfig(services);
            ConfigureService.AddDependenciesService(services);
            ConfigureRepository.AddDependenciesRepository(services, Configuration);
            ConfigureAutoMapper<Startup>.AddAutoMapperConfig(services);
            ConfigureSwagger.AddSwaggerConfig(services);            
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureMiddlewares.AddMiddlewaresConfig(app, env);
        }
    }
}
