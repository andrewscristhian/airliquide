﻿using AirLiquide.Domain.Models;
using AirLiquide.Infra.Data.Context;
using AirLiquide.Service.Services;
using AirLiquide.Test.UnitTest.Data.Base;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using static AirLiquide.Test.UnitTest.Data.Base.BaseTest;

namespace AirLiquide.Test.UnitTest.Data.Entities
{
    public class CustomerTests : BaseTest, IClassFixture<DbTest>
    {
        private ServiceProvider _serviceProvider;

        public CustomerTests(DbTest dbTest)
        {
            _serviceProvider = dbTest.ServiceProvider;
        }

        [Fact(DisplayName = "CRUD Customer")]
        [Trait("CRUD", "Customer")]
        public async Task Customer_CRUD()
        {
            using (var context = _serviceProvider.GetService<AirLiquideContext>())
            {
                CustomerService _service = new CustomerService(context);

                // Criação
                Customer customer = new Customer
                {
                    Name = Faker.Name.First(),
                    Age = Faker.RandomNumber.Next()
                };

                var _createdRecord = await _service.InsertAsync(customer);
                Assert.NotNull(_createdRecord);
                Assert.Equal(customer.Name, _createdRecord.Name);
                Assert.Equal(customer.Age, _createdRecord.Age);
                Assert.False(_createdRecord.Id == Guid.Empty);

                // Atualização
                customer.Name = Faker.Name.First();
                customer.Age = Faker.RandomNumber.Next();

                var _updatedRecord = await _service.UpdateAsync(customer);
                Assert.NotNull(_updatedRecord);
                Assert.Equal(customer.Name, _updatedRecord.Name);
                Assert.Equal(customer.Age, _updatedRecord.Age);

                //Verificar registro existe
                var _existRecord = _service.ExistAsync(_updatedRecord.Id);
                Assert.True(await _existRecord);

                // Selecionar por Id
                var _recordById = await _service.GetAsync(_updatedRecord.Id);
                Assert.NotNull(_recordById);
                Assert.Equal(_updatedRecord.Name, _recordById.Name);
                Assert.Equal(_updatedRecord.Age, _recordById.Age);

                // Selecionar todos os registros
                var _allRecords = await _service.GetAllAsync();
                Assert.NotNull(_allRecords);
                Assert.True(_allRecords.Count() > 0);

                // Deleção
                var _removedRecord = await _service.DeleteAsync(_recordById);
                Assert.True(_removedRecord);
            }
        }
    }
}
