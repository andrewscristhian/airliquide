using AirLiquide.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AirLiquide.Test.UnitTest.Data.Base
{
    public abstract class BaseTest
    {
        public BaseTest()
        { }

        public class DbTest : IDisposable
        {
            private string dbName = $"AirLiquideDBTest_{Guid.NewGuid().ToString().Replace("-", string.Empty)}";
            public ServiceProvider ServiceProvider { get; private set; }

            public DbTest()
            {
                var serviceCollection = new ServiceCollection();
                serviceCollection.AddDbContext<AirLiquideContext>(c =>
                    c.UseSqlServer($"Persist Security Info=True;Server=ANDREWS\\SQLEXPRESS;Database={dbName};Trusted_Connection=true;MultipleActiveResultSets=true"),
                    ServiceLifetime.Transient
                );

                ServiceProvider = serviceCollection.BuildServiceProvider();
                using (var context = ServiceProvider.GetService<AirLiquideContext>())
                {
                    context.Database.EnsureCreated();
                }
            }

            public void Dispose()
            {
                using (var context = ServiceProvider.GetService<AirLiquideContext>())
                {
                    context.Database.EnsureDeleted();
                }
            }
        }
    }
}
